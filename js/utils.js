class JWT {
  static parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join('')
    );

    return JSON.parse(jsonPayload);
  }
}

class Validator {
  static validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.trim().toLowerCase());
  }

  static validatePassLength(password, min, max) {
    return password.trim().length > min && password.trim().length < max;
  }

  static confirmPassword(password, password2) {
    return password === password2;
  }
}

class Fetch {
  static async getGuitars() {
    try {
      const res = await fetch('http://52.170.37.60/guitars');
      const data = await res.json();
      return data;
    } catch (err) {
      return [];
    }
  }

  static async postSignIn(user) {
    try {
      const res = await fetch('http://52.170.37.60/users/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: `email=${user.email}&password=${user.password}`,
      });
      const token = await res.json();
      sessionStorage.setItem('token', token);

      return true;
    } catch (err) {
      return false;
    }
  }

  static async postSignUp(user) {
    try {
      const res = await fetch('http://52.170.37.60/users/signup', {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: `email=${user.email}&password=${user.password}`,
      });
      const msg = await res.text();

      return msg === 'Success';
    } catch (err) {
      return false;
    }
  }

  static async getCart(token) {
    const userId = JWT.parseJwt(token).id;
    try {
      const res = await fetch(`http://52.170.37.60/users/${userId}/cart`, {
        headers: { Authorization: token },
      });
      const data = await res.json();
      return data;
    } catch (err) {}
  }

  static async postAddToCart(token, guitarId) {
    const userId = JWT.parseJwt(token).id;
    if (userId) {
      try {
        const res = await fetch(
          `http://52.170.37.60/users/${userId}/cart/${guitarId}`,
          {
            method: 'POST',
            headers: { Authorization: token },
          }
        );
        const msg = await res.text();
        return msg;
      } catch (err) {}
    }
  }

  static async deleteFromCart(token, guitarId) {
    const userId = JWT.parseJwt(token).id;
    if (userId) {
      try {
        const res = await fetch(
          `http://52.170.37.60/users/${userId}/cart/${guitarId}`,
          {
            method: 'DELETE',
            headers: { Authorization: token },
          }
        );
        const msg = await res.text();
        return msg;
      } catch (err) {}
    }
  }

  static async postPurchase(token) {
    const userId = JWT.parseJwt(token).id;

    if (userId) {
      try {
        const res = await fetch(
          `http://52.170.37.60/users/${userId}/purchase`,
          {
            method: 'POST',
            headers: { Authorization: token },
          }
        );
        const msg = await res.text();
        return msg;
      } catch (err) {}
    }
  }
}
