class Init {
  constructor() {}

  async validateToken(token) {
    const userId = JWT.parseJwt(token).id;
    if (userId) {
      try {
        const res = await fetch(`http://52.170.37.60/users/${userId}/cart`, {
          headers: { Authorization: token },
        });
        const data = await res.json();
        return data;
      } catch (err) {}
    }
  }

  async renderLandingPage() {
    const token = sessionStorage.getItem('token');

    if (token) {
      const cartData = await this.validateToken(token);

      if (cartData) {
        // Signed in!
        this.renderSignedInComponents(cartData);
      } else {
        // Invalid token!
        sessionStorage.removeItem('token');

        this.renderDefaultComponents();
      }
    } else {
      // No token in sessionStorage!
      this.renderDefaultComponents();
    }
  }

  renderDefaultComponents() {
    Nav.appendDefaultNav();
    HeroSection.appendSignUpBtn();
    new SignInForm();
    new SignUpForm();
  }

  renderSignedInComponents(cartData) {
    Nav.appendUserPageNav(cartData);
    HeroSection.appendJokeAPIBtn();
    new SignOut();
  }
}

new Init().renderLandingPage();
