class CartItemsPortal {
  static overlay = document.getElementById('cart-items-overlay');

  static appendCartItemsPortal(cartData) {
    let totalPrice = 0;
    cartData.forEach(cartItem => (totalPrice += cartItem[2]));
    this.overlay.innerHTML = '';
    this.overlay.style.display = 'block';
    this.overlay.innerHTML = `
    <div class="cart-items-portal">
        <h2>CHECK OUT</h2>
        <div class="purchase-container">
          <div class="items">
            <ul id="cart-item-list">
            </ul>
            <div class="total-container">
              <div class="continue-shopping">Continue shopping</div>
              <div class="total"><span>Total:</span> $${totalPrice}</div>
            </div>
          </div>
          <div class="purchase">
            <div class="radio-container">
              <input type="radio" id="credit" name="payment-method" />
              <label for="credit">Credit Card</label><br />
              <input type="radio" id="paypal" name="payment-method" />
              <label for="paypal">PayPal</label><br />
              <input type="radio" id="other" name="payment-method" />
              <label for="other">Other</label>
            </div>
            <div class="purchase-btn">Purchase</div>
          </div>
        </div>
        <div class="portal-exit" id="cart-items-portal-exit">
          <i class="fas fa-times"></i>
        </div>
      </div>
    `;

    this.prepareExitPortal();
    cartData.forEach(cartItem => this.appendCartItem(cartItem));

    document
      .querySelector('.cart-items-portal .continue-shopping')
      .addEventListener('click', () => {
        window.location.href = 'shop.html';
      });

    document
      .querySelector('.cart-items-portal .purchase-btn')
      .addEventListener('click', this.clickPurchase.bind(this));
  }

  static async clickPurchase() {
    const token = sessionStorage.getItem('token');
    const msg = await Fetch.postPurchase(token);

    if (msg === 'Successfully purchased') {
      const cartData = await Fetch.getCart(token);
      this.overlay.innerHTML = '';
      this.overlay.style.display = 'none';
      Nav.appendUserPageNav(cartData);

      AddToCartPortal.appendSuccessfulMsg('Successfully purchased!');
    } else {
      this.appendErrMsg('Failed to purchase');
    }
  }

  static appendCartItem(cartItem) {
    const guitarId = cartItem[0];
    const cartItemList = document.getElementById('cart-item-list');
    const listEl = document.createElement('li');
    listEl.innerHTML = `
      ${cartItem[1]} x${cartItem[3]}
      <div class="price-container">
        <div class="remove-btn" id="remove-btn${guitarId}">REMOVE</div>
        $${cartItem[2] * cartItem[3]}
      </div>
    `;

    cartItemList.appendChild(listEl);

    document
      .getElementById(`remove-btn${cartItem[0]}`)
      .addEventListener('click', async e => {
        const token = sessionStorage.getItem('token');
        const msg = await Fetch.deleteFromCart(token, guitarId);

        if (msg === 'Successfully deleted') {
          const token = sessionStorage.getItem('token');
          const cartData = await Fetch.getCart(token);
          this.appendCartItemsPortal(cartData);
          Nav.appendUserPageNav(cartData);
        } else {
          this.appendErrMsg('Failed to remove');
        }
      });
  }

  static appendErrMsg(msg) {
    const cartItemsPortal = this.overlay.querySelector('.cart-items-portal');

    cartItemsPortal.insertAdjacentHTML(
      'beforeend',
      `<div class="failed-to-remove">${msg}</div>`
    );

    setTimeout(() => {
      cartItemsPortal.removeChild(
        document.querySelector('.cart-items-portal .failed-to-remove')
      );
    }, 3000);
  }

  static prepareExitPortal() {
    document
      .getElementById('cart-items-portal-exit')
      .addEventListener('click', () => {
        this.overlay.innerHTML = '';
        this.overlay.style.display = 'none';
      });
  }
}

class Nav {
  static header = document.querySelector('header');

  static appendDefaultNav() {
    this.header.insertAdjacentHTML(
      'beforeend',
      `<nav>
          <ul>
            <li>
              <a href="index.html">Home</a>
              <div class="underline"></div>
            </li>
            <li>
              <a href="shop.html">Shop</a>
              <div class="underline"></div>
            </li>
            <li class="nav-sign-in" id="nav-sign-in"><a href="#">Sign In</a></li>
            <li id="nav-cart">
              <a href="#"><i class="fas fa-shopping-cart"></i></a>
            </li>
          </ul>
        </nav>`
    );

    const navCart = document.getElementById('nav-cart');

    navCart.addEventListener('click', () => {
      navCart.insertAdjacentHTML(
        'beforeend',
        `
      <div class="sign-in-required-msg">Please sign in</div>`
      );

      setTimeout(() => {
        navCart.removeChild(document.querySelector('.sign-in-required-msg'));
      }, 3000);
    });
  }

  static appendUserPageNav(cartData) {
    const nav = document.querySelector('nav');
    nav && this.header.removeChild(nav);
    this.header.insertAdjacentHTML(
      'beforeend',
      `<nav>
            <ul>
              <li>
                <a href="index.html">Home</a>
                <div class="underline"></div>
              </li>
              <li>
                <a href="shop.html">Shop</a>
                <div class="underline"></div>
              </li>
              <li class="nav-sign-out" id="nav-sign-out"><a href="index.html">Sign Out</a></li>
              <li id="nav-cart">
                <a href="#"><i class="fas fa-shopping-cart"></i></a>
                <div class="cart-item-count" id="cart-item-count">${cartData.length}</div>
              </li>
            </ul>
          </nav>`
    );

    document.getElementById('nav-cart').addEventListener('click', () => {
      CartItemsPortal.appendCartItemsPortal(cartData);
    });
  }
}

class HeroSection {
  static heroTextContainer = document.querySelector('.hero .text-container');

  static jokeCount = 0;

  static appendSignUpBtn() {
    this.heroTextContainer.insertAdjacentHTML(
      'beforeend',
      `<div class="hero-sign-up" id="hero-sign-up">Sign Up</div>`
    );
  }

  static appendJokeAPIBtn() {
    this.heroTextContainer.insertAdjacentHTML(
      'beforeend',
      `<div class="hero-sign-up" id="joke-btn">Tell Me A Programming Joke</div>`
    );

    document.getElementById('joke-btn').addEventListener('click', async () => {
      const joke = await this.fetchJoke();

      if (!this.jokeCount) {
        this.appendJoke(joke);
        this.jokeCount++;
      } else {
        this.replaceJoke(joke);
      }
    });
  }

  static async fetchJoke() {
    const res = await fetch(
      'https://v2.jokeapi.dev/joke/Programming?blacklistFlags=nsfw,religious,racist,sexist,explicit&type=single'
    );
    const jokeData = await res.json();
    return jokeData.joke;
  }

  static appendJoke(joke) {
    this.heroTextContainer.insertAdjacentHTML(
      'beforeend',
      `<p class="joke">${joke}</p>`
    );
  }

  static replaceJoke(joke) {
    document.querySelector('.hero .text-container .joke').remove();

    this.heroTextContainer.insertAdjacentHTML(
      'beforeend',
      `<p class="joke">${joke}</p>`
    );
  }
}

class SearchBox {
  constructor() {
    this.searchInput = document.getElementById('search-input');
    this.searchBtn = document.querySelector('.search-box .search-btn');

    this.searchBtn.addEventListener('click', this.clickSearch.bind(this));
  }

  clickSearch() {
    const searchValue = this.searchInput.value.trim().toLowerCase();
    Card.cardContainer.innerHTML = '';
    Card.renderFilteredCards(searchValue);
  }
}

class FilterGuitars {
  constructor() {
    this.brandFilterContainer = document.querySelector(
      '.filter-section .brand-filter-container'
    );
    this.brandFilters = this.brandFilterContainer.querySelectorAll('.filter');

    this.priceFilterContainer = document.querySelector(
      '.filter-section .price-filter-container'
    );
    this.priceFilters = this.priceFilterContainer.querySelectorAll('.filter');

    this.conditionFilterContainer = document.querySelector(
      '.filter-section .condition-filter-container'
    );
    this.conditionFilters = this.conditionFilterContainer.querySelectorAll(
      '.filter'
    );

    this.initiallyStoredBrandFilters = JSON.parse(
      sessionStorage.getItem('brand-filter')
    );
    this.initiallyStoredPriceFilters = JSON.parse(
      sessionStorage.getItem('price-filter')
    );

    this.initiallyStoredConditionFilters = JSON.parse(
      sessionStorage.getItem('condition-filter')
    );

    if (this.initiallyStoredBrandFilters) {
      for (const filter of this.brandFilters) {
        const filterText = filter.innerText.toLowerCase();
        if (this.initiallyStoredBrandFilters.includes(filterText))
          filter.classList.add('active');
      }
    }

    if (this.initiallyStoredPriceFilters) {
      for (const filter of this.priceFilters) {
        const filterText = filter.id;
        if (this.initiallyStoredPriceFilters.includes(filterText))
          filter.classList.add('active');
      }
    }

    if (this.initiallyStoredConditionFilters) {
      for (const filter of this.conditionFilters) {
        const filterText = filter.innerText.toLowerCase();
        if (this.initiallyStoredConditionFilters.includes(filterText))
          filter.classList.add('active');
      }
    }

    this.brandFilters.forEach(filter =>
      filter.addEventListener('click', this.clickBrandFilter.bind(this))
    );
    this.priceFilters.forEach(filter =>
      filter.addEventListener('click', this.clickPriceFilter.bind(this))
    );
    this.conditionFilters.forEach(filter =>
      filter.addEventListener('click', this.clickConditionFilter.bind(this))
    );
  }

  clickBrandFilter(e) {
    const newFilterWord = e.target.innerText.toLowerCase();
    const storedFilters = JSON.parse(sessionStorage.getItem('brand-filter'));

    if (storedFilters) {
      if (storedFilters.filter(keyWord => keyWord === newFilterWord).length) {
        e.target.classList.remove('active');
        sessionStorage.setItem(
          'brand-filter',
          JSON.stringify(
            storedFilters.filter(keyWord => keyWord !== newFilterWord)
          )
        );
      } else {
        e.target.classList.add('active');
        sessionStorage.setItem(
          'brand-filter',
          JSON.stringify(storedFilters.concat([newFilterWord]))
        );
      }
    } else {
      e.target.classList.add('active');
      sessionStorage.setItem('brand-filter', JSON.stringify([newFilterWord]));
    }
  }

  clickPriceFilter(e) {
    const newFilterPrice = e.target.id;
    const storedFilters = JSON.parse(sessionStorage.getItem('price-filter'));

    if (storedFilters) {
      if (storedFilters.filter(price => price === newFilterPrice).length) {
        e.target.classList.remove('active');
        sessionStorage.setItem(
          'price-filter',
          JSON.stringify(
            storedFilters.filter(price => price !== newFilterPrice)
          )
        );
      } else {
        e.target.classList.add('active');
        sessionStorage.setItem(
          'price-filter',
          JSON.stringify(storedFilters.concat([newFilterPrice]))
        );
      }
    } else {
      e.target.classList.add('active');
      sessionStorage.setItem('price-filter', JSON.stringify([newFilterPrice]));
    }
  }

  clickConditionFilter(e) {
    const newFilterWord = e.target.innerText.toLowerCase();
    const storedFilters = JSON.parse(
      sessionStorage.getItem('condition-filter')
    );

    if (storedFilters) {
      if (storedFilters.filter(keyWord => keyWord === newFilterWord).length) {
        e.target.classList.remove('active');
        sessionStorage.setItem(
          'condition-filter',
          JSON.stringify(
            storedFilters.filter(keyWord => keyWord !== newFilterWord)
          )
        );
      } else {
        e.target.classList.add('active');
        sessionStorage.setItem(
          'condition-filter',
          JSON.stringify(storedFilters.concat([newFilterWord]))
        );
      }
    } else {
      e.target.classList.add('active');
      sessionStorage.setItem(
        'condition-filter',
        JSON.stringify([newFilterWord])
      );
    }
  }
}

class AddToCartPortal {
  static overlay = document.getElementById('add-to-cart-overlay');

  static appendPortal(guitarData) {
    this.overlay.innerHTML = `
      <div class="add-to-cart-portal">
        <h3 class="name">${guitarData.name} (${guitarData.maker})</h3>
        <div class="btn-container">
          <h3 class="price">$${guitarData.price} (${guitarData.condition})</h3>
          <button id="add-to-cart-btn">Add To Cart</button>
        </div>
        <div class="portal-exit" id="add-to-cart-portal-exit">
          <i class="fas fa-times"></i>
        </div>
      </div>
    `;
    this.overlay.style.display = 'block';

    const addBtn = document.getElementById('add-to-cart-btn');
    const exitPortalBtn = document.getElementById('add-to-cart-portal-exit');
    addBtn.addEventListener('click', () => {
      this.clickAddToCart(guitarData.id);
    });
    exitPortalBtn.addEventListener('click', this.clickExitPortalBtn.bind(this));
  }

  static async clickAddToCart(guitarId) {
    const token = sessionStorage.getItem('token');
    if (token) {
      const msg = await Fetch.postAddToCart(token, guitarId);

      if (msg === 'Successfully added') {
        const cartData = await Fetch.getCart(token);
        this.overlay.innerHTML = '';
        this.overlay.style.display = 'none';
        this.appendSuccessfulMsg('Successfully added!');
        Nav.appendUserPageNav(cartData);
      } else {
        this.appendErrMsg('Failed to add to cart');
      }
    } else {
      this.appendErrMsg('Please sign in');
    }
  }

  static clickExitPortalBtn() {
    this.overlay.innerHTML = '';
    this.overlay.style.display = 'none';
  }

  static appendErrMsg(msg) {
    const addToCartPortal = this.overlay.querySelector('.add-to-cart-portal');

    addToCartPortal.insertAdjacentHTML(
      'beforeend',
      `<div class="add-to-cart-please-sign-in">${msg}</div>`
    );

    setTimeout(() => {
      addToCartPortal.removeChild(
        document.querySelector('.add-to-cart-please-sign-in')
      );
    }, 3000);
  }

  static appendSuccessfulMsg(msg) {
    const successMsgDiv = document.createElement('div');
    successMsgDiv.classList.add('successfully-added');
    successMsgDiv.innerText = msg;
    document.body.appendChild(successMsgDiv);

    setTimeout(() => {
      document.body.removeChild(successMsgDiv);
    }, 3000);
  }
}

class Card {
  static cardContainer = document.querySelector(
    '.product-section .card-container'
  );

  static searchResultNumber = document.getElementById('search-result-number');

  static appendCard(cardData) {
    const cardDiv = document.createElement('div');
    cardDiv.classList.add('card');
    cardDiv.innerHTML = `
    <h3 class="name">${cardData.name}</h3>
    <div class="guitar-image-container"><img src="images/guitar.png" alt="guitar" /><h3>${cardData.maker}</h3></div>
    <h3 class="price">$${cardData.price} (${cardData.condition})</h3>
    `;

    this.cardContainer.appendChild(cardDiv);

    cardDiv.addEventListener('click', () => {
      AddToCartPortal.appendPortal(cardData);
    });
  }

  static async renderCards() {
    const guitars = await Fetch.getGuitars();
    guitars.forEach(data => this.appendCard(data));
  }

  static async renderFilteredCards(searchValue) {
    searchValue = searchValue.toLowerCase();

    const storedBrandFilterWords = JSON.parse(
      sessionStorage.getItem('brand-filter')
    );
    const storedPriceFilterWords = JSON.parse(
      sessionStorage.getItem('price-filter')
    );
    const storedConditionFilterWords = JSON.parse(
      sessionStorage.getItem('condition-filter')
    );

    const guitars = await Fetch.getGuitars();

    let filteredGuitars = guitars.filter(guitarData =>
      guitarData.name.toLowerCase().includes(searchValue)
    );

    if (storedBrandFilterWords && storedBrandFilterWords.length) {
      let brandFilteredGuitars = [];

      for (const brandName of storedBrandFilterWords) {
        brandFilteredGuitars = brandFilteredGuitars.concat(
          filteredGuitars.filter(
            guitarData => guitarData.maker.toLowerCase() === brandName
          )
        );
      }

      filteredGuitars = brandFilteredGuitars;
    }

    if (storedPriceFilterWords && storedPriceFilterWords.length) {
      let priceFilteredGuitars = [];

      for (const price of storedPriceFilterWords) {
        switch (price) {
          case '500':
            priceFilteredGuitars = priceFilteredGuitars.concat(
              filteredGuitars.filter(guitarData => guitarData.price < 500)
            );
            break;

          case '1000':
            priceFilteredGuitars = priceFilteredGuitars.concat(
              filteredGuitars.filter(
                guitarData => guitarData.price >= 500 && guitarData.price < 1000
              )
            );
            break;

          case '1500':
            priceFilteredGuitars = priceFilteredGuitars.concat(
              filteredGuitars.filter(
                guitarData =>
                  guitarData.price >= 1000 && guitarData.price < 1500
              )
            );
            break;

          case '2000':
            priceFilteredGuitars = priceFilteredGuitars.concat(
              filteredGuitars.filter(
                guitarData =>
                  guitarData.price >= 1500 && guitarData.price < 2000
              )
            );
            break;

          case 'over':
            priceFilteredGuitars = priceFilteredGuitars.concat(
              filteredGuitars.filter(guitarData => guitarData.price >= 2000)
            );
            break;

          default:
            console.log('How did this happen...?');
        }
      }

      filteredGuitars = priceFilteredGuitars;
    }

    if (storedConditionFilterWords && storedConditionFilterWords.length) {
      let conditionFilteredGuitars = [];

      for (const condition of storedConditionFilterWords) {
        conditionFilteredGuitars = conditionFilteredGuitars.concat(
          filteredGuitars.filter(
            guitarData => guitarData.condition === condition
          )
        );
      }

      filteredGuitars = conditionFilteredGuitars;
    }

    this.searchResultNumber.innerText = filteredGuitars.length + ' results';

    filteredGuitars.forEach(data => this.appendCard(data));
  }
}
