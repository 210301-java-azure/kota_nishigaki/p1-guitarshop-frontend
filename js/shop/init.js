class Init {
  constructor() {}

  async validateToken(token) {
    const userId = JWT.parseJwt(token).id;
    if (userId) {
      return await Fetch.getCart(token);
    }
  }

  async renderShopPage() {
    const token = sessionStorage.getItem('token');

    if (token) {
      const cartData = await this.validateToken(token);

      if (cartData) {
        this.renderSignedInComponents(cartData);
      } else {
        sessionStorage.removeItem('token');
        console.error('Token is invalid');

        this.renderDefaultComponents();
      }
    } else {
      this.renderDefaultComponents();
    }
  }

  renderDefaultComponents() {
    Nav.appendDefaultNav();
    Card.renderFilteredCards('');
    new SearchBox();
    new SignInForm();
    new SignUpForm();
    new FilterGuitars();
  }

  renderSignedInComponents(cartData) {
    Nav.appendUserPageNav(cartData);
    Card.renderFilteredCards('');
    new SearchBox();
    new SignOut();
    new FilterGuitars();
  }
}

new Init().renderShopPage();
